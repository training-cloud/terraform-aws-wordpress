provider "aws" {
  version                 = "~> 3.34"
  region                  = var.aws_reg
  profile                 = "nubiral"
  shared_credentials_file = "~/.aws/credentials"
}
