### terraform-aws-wordpress

By Rossana Suarez #RoxsRoss

Creating infrastructure by terraform for Wordpress 
=========
Generate a pair of SSH keys somewhere you remember, for example, in the keys folder at the root of your repo:

$ mkdir keys
$ ssh-keygen -q -f keys/aws_terraform -C aws_terraform_ssh_key -N ''


Set up wordpress in AWS infrastructure. Using terraform to provision infrastructure. Code uses and creates following aws services:

1. VPC and it's components
2. Subnets, Route Tables, Internet Gateway, Nat Gateway.
3. EC2 instance
4. EIP for NAT Gateway
5. RDS mysql instance.
6. Security Groups to access both EC2 and MYSQL
7. ALB

Edit PATH in vars.tf for your keys and backend.tf
