terraform {
  backend "s3" {
    bucket         = "training-gitlab-tf"
    region         = "us-east-1"
    encrypt        = true
    key            = "wordptf/deploy.tfstate"
    shared_credentials_file = "~/.aws/credentials"
  }
}