
# ========================================================================= ALB
resource "aws_alb" "wordpress-alb" {
  name                = "${var.project}-alb"
  internal            = false
  load_balancer_type  = "application"
  subnets             = "${aws_subnet.wordpress_public_subnet.*.id}"
  security_groups     = ["${aws_security_group.alb-sg.id}"]
  tags   = {
    Name = "${var.project}-alb"
  }
}

# ========================================================================= Target Group

resource "aws_lb_target_group_attachment" "wordpress-target-group-attachment" {
  target_group_arn = "${aws_alb_target_group.wordpress-group.arn}"
  target_id        = "${aws_instance.wordpress-ec2.id}"
  port             = 80
}

resource "aws_alb_target_group" "wordpress-group" {
  name     = "terraform-example-alb-target"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.wordpress_vpc.id}"
  slow_start = 30
}



resource "aws_alb_listener" "wordpress-listener-http" {
  load_balancer_arn = "${aws_alb.wordpress-alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.wordpress-group.arn}"
    type             = "forward"
  }
}

